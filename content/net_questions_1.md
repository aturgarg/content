C# /ASP.net/jQuery/MVC/.Net/Design pattern 
1) What is the difference between string and string builder?
2) User control : How to do postback through web page?
3) What are and delegate events? 
4) How to use delegate and event in user control?
5) MVC and asp.net difference. Why to choose MVC if ASP.net fulfils my project requirements?
6) Partial views and strongly typed view difference.
7) What is master page?
8) How to do javascript client side validation? Why we do validation at client side?
9) How to use jquery in textbox bind event?
10) How to use observer design pattern for user control?
11) How to implement singleton and façade pattern?
12) What are limitations of Open XML?
13) What is difference between a.equals(b) and a==b?
14) What is the performance issue in for loop?
15) What is Nuget?
16) If I want to do debugging in Jquery then which library I need to add in my project?
17) What is the yield and its purpose in C#?
18) Have you used any collections in C#? What is the purpose of using it?
19) How to find whether data is stored in stack or its in heap?
20) How registry is maintained?
21)What are advantages of Interop?
22) How to use satellite assemblies? Have you ever thought of using it in your project?
23) How to improve performance of code in C#? Do you follow any techniques?
24) What is publisher subscriber model? Any design pattern can be implemented for it?
SQL section: 
1)    What is the difference between truncate and delete? 
2)	How to count of inserted rows without select statement? 
3)	What is SQL bulk copy? 
4)	What is performance tuning in SQL? 
5)	How you can retrieve data which is case sensative using SQL query?
6)	Are there any index present other than clustered and non-clustered index?
7)	How to find employee's data alongwith its manager's data if you have table which has following columns empid , manager's id , name,salary, designation etc.