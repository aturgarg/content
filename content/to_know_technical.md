Understanding of different model of UML/UML diagram.
Ability to convert UML diagram into C# Code and Vice Versa
"Understanding of different layer of dot net framework. 
Understanding of platform independent language. 
Understanding of CLR.
Understanding of source code compilation into IL.
Ability to understand the architecture diagram of dot net framework."
"Ability to understand the concept of managed/unmanaged code. 
Ability to handle the Implementation of managed/unmanaged resources in code. 
Understanding of dispose and finalize methods.
"
"Knowledge of  IL.
Understanding of metadata. 
Knowledge of conversion of source code into IL.
"
Ability to write the code to  access the metadata using the reflection.
"Ability to declare the variables, 
Ability to Typecast the variable"
"Knowledge of Value type and reference type
Knowledge of boxing and unboxing
Ability to pass parameters as value type and reference type"
"Ability to use String and StringBuilder Class
Ability to differenciate between ASCII and UNICODE"
"Ability to write/define Main Method of program
Ability to pass and use the Parameters to Main method"
Ability to define/write a function accepting Value Type / Reference Type Parameter and Returning the Value
Understanding of the different Access Modifier 
Understanding of named methods used in delegate. Ability to implement the delegate in the code.
"Understanding of Try and Catch block
Exception logging and Tracing
Handling Exception using event log"
Ability to implement the exception handling classes and mehods in project.
"Understanding The concept of Logical and Physical partition of code,
Ability to access methods  using namespace
Understanding of basic system namespace"
"Ability to have the basic knowledge of the read and write from console and basic methods of IO Namespace. 
Ability to read/write the file to/from the disk."
"Ability to use the collection object like array, array list, hash table and sorted array list in the project. 
Ability to use the generic collection like Ilist, List , Idictionary in project."
Ability to write a function using different access modifiers and use the funcitons/properties in code
ability to write a default constructor
Ability to write parameterized constructor and overloading of constructor
"Understanding the concept of Abstraction and Polymorphism
Knowledge of different type of Polymorphism
Ability to use Abstract Class and Interface.
Ability to implement the drive class."
Ability to initialize the object of class and use this in code.
Ability to implement the parametrized constructor.
Ability to differentiate between value type and reference type.
Ability to implement the boxing and unboxing in code.
Ability to call a method and access the properties/variable of the method.
Understanding of the concept of abstact class, sealed class, inheritance and different type of class modifiers.
Ability to implement the different collection classes like array, arraylist, hashtable, dictionary, generics, sorted array list.
Ability to implement the code using generics.
Ability to declare the interface.
Ability to implement the interface.
Ability to implement the runtime polimorphism in interface.
Ability to implement the event/delegates in code. Understanding of different type of delegate like mulicast and single cast delegate. 
Understanding the basic knowledge of assembly and manifest.
Understanding of the deployment of web/window application. Ability to deploy the web/window application in N-Layer and N-Tier environment.
Ability to use ILDASM which is used to get the meta data about the assembly.
Ability to create a custom control like gridview, textbox etc. and use these in the code.
Ability to use the System.ComponentModel.Container  class and add custom functionality to enforce rules, override base methods, or any other custom functionality you want your container to incorporate.
Add custom functionality in the ComponentModel.Container class to enforce rules, override base methods, or any other custom functionality
Abitlity to use the COM/DCOM componant functions and property from code project.
Understanding of tlbImport and tlbexport utility to use com componant in dot net and use dot net comonant in COM application.
Use COM componant in dot net using wrapper classes. Implement the wrapper for using COM functionality.
Ability to handle cross language difference, Accessing metadata 
Able to write the code to access Medadata using reflection
Ability to create Windows /Web UI using general control
Ability to create Windows /Web UI using Custom control
Ablity to read and write to external file
Ability to write code for serialisation and de-serialisation
Ability to use LINQ for accessing data from database, object collection, iterate xml document etc.
Ability to use lamda expression with LINQ
Knowledge of Different Tiers/layers of C# Application
Common language Infrastructure standards
Knowledge of different features in different C# standards
Understanding of the IEnumerable  interface. Ability to use this interface and its methods and property in code.
Understanding of the extension methods. Understanding of the pros/cons of extension methods.
Ability to implement the extension method in the code. 
Understanding of the lamda expression.
Ability to Implement the extension method
Understanding of threading concept
Understanding of debug and tracing concept
Understanding of file read, input, output and write the file on the disk
Understanding of the basis fundamental of C#
Understanding of the application domain
Understanding of security concept in C#